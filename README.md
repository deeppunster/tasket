# Tasket

A command line app to track your to-dos, tasks, checklists, and other needful things.

Renamed to "tasket" due to an unfortunate namespace collision.

# Installation

pip install tasket

# Requirements

Python 3.6 or later.


