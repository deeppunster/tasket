from setuptools import setup

setup(
    name='tasket',
    version='0.0.2',
    description='A command line app to track your to-dos, tasks, checklists, and other needful things.',
    long_description='A command line app to track your to-dos, tasks, checklists, and other needful things.',
    url='https://gitlab.com/damiencalloway/tasket',
    author='Damien Calloway',
    author_email='cheeseshop@damiencalloway.com',
    readme = "README.md",
    packages=[],
    classifiers=["Development Status :: 2 - Pre-Alpha",
                 "Environment :: Console",
                 "License :: OSI Approved :: BSD License"
                 ],
)
